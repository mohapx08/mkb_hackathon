import torch
import numpy as np


class EarlyStopping:
    def __init__(self, patience=20, mode='min', verbose=False, delta=0, metric_name=None):
     
        if mode not in ['min', 'max']:
            raise ValueError(f'Unrecognized mode: {mode}!')

        self.patience = patience
        self.best_weigts = {}
        self.mode = mode
        self.verbose = verbose
        self.counter = 0
        self.best_score = None
        self.early_stop = False
        self.best_prev_score = np.Inf if mode == 'min' else -np.Inf
        self.delta = delta
        self.metric_name = 'metric' if not metric_name else metric_name

    def __call__(self, metric_value, model):

        score = -metric_value if self.mode == 'min' else metric_value

        if self.best_score is None:
            self.best_score = score
            self.save_checkpoint(metric_value, model)
        elif score < self.best_score + self.delta:
            self.counter += 1
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            self.best_score = score
            self.save_checkpoint(metric_value, model)
            self.counter = 0

    def save_checkpoint(self, metric_value, model):
        """Saves model when validation loss decrease."""
        if self.verbose:
            print(
                f'Validation {self.metric_name} improved ({self.best_prev_score:.6f} --> {metric_value:.6f}).  Saving model ...')
        self.best_weigts = model.state_dict()
        self.best_prev_score = metric_value
