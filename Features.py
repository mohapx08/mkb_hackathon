import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from datetime import datetime as dt
    
def feature_process(frame) -> pd.DataFrame:

    app_float = list(filter(lambda x: frame[x].dtypes != 'O', frame.columns))
    app_obj = list(filter(lambda x: frame[x].dtypes == 'O', frame.columns))
    app_data = list(filter(lambda x: 'DATE' in x, app_obj)) # список всех колонок с датами
    app_str = list(filter(lambda x: 'DATE' not in x, app_obj)) # список всех колонок без дат
    for i in app_data:
        frame[i] = pd.to_datetime(frame[i], format='%d%b%Y:%H:%M:%S').dt.strftime('%m.%Y') 
    df_duplic_id = pd.DataFrame(frame.id_client.value_counts()).reset_index().rename(columns={'index': 'id_client', 'id_client': 'cnt'})
    frame = pd.merge(
                        frame,
                        df_duplic_id,
                        how="left",
                        on='id_client',
                        right_index=False,
                        validate='many_to_one'
                    )
    # столбцы, где есть нулевые значения и значения числовые
    app_null = list(filter(lambda x: (frame[x].isin([0]).any() == True) and (frame[x].dtypes != 'O') , frame.columns))
    # столбцы, где нет нулевых значений и значения числовые
    app_nonull = list(filter(lambda x: (frame[x].isin([0]).any() == False) and (frame[x].dtypes != 'O') , frame.columns))
    frame[app_nonull] = frame[app_nonull].fillna(0)# Заполняем пропущенные значения из столбцов app_nonull нулевыми значениями
    # Преобразуем некоторые значения обратно в код ОКВЕД, так как Excel сразу их форматирует на дату 
    index_data_okved = list(filter(lambda x: type(frame.OKVED_CODE[x]) == dt , frame.index))
    for i in index_data_okved:
        frame.loc[i, 'OKVED_CODE'] = frame.OKVED_CODE.loc[i].strftime('%m.%y')
    # Преобразуем код ОКВЕД в строку и форматируем до 4-х значащих цифр
    index_str_okved = list(filter(lambda x: type(frame.OKVED_CODE[x]) == str , frame.index))
    for i in index_str_okved:
        frame.loc[i, 'OKVED_CODE'] = str(frame.OKVED_CODE.loc[i][:5])
    frame[app_str] = frame[app_str].fillna('unknown')
    frame[app_data] = frame[app_data].fillna(frame[app_data].median())
    frame = frame.drop(['TAXREGPAY_REGDATE', 'BIRTHDATE'], axis =1) #TAXREGPAY_REGDATE неинфомрмативна
    
    return frame
    